const mongoose = require('mongoose');
const Joi = require('joi');

const userSchema = new mongoose.Schema({

	firstName : {
		type:String,
		required: [true, "firstname name is required"]
	},
	lastName: {
		type: String,
		required: [true, "lastname is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"],
		unique: true
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile no is required"]
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "CourseId is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type:String,
				default: "Enrolled"
			}
		}

	]
})



function validateUser(user) {
  const schema = {
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string().required().email(),
    mobileNo: Joi.string().required(),
    password: Joi.string().required().unique()
  };

  return Joi.validate(user, schema);
}


module.exports = mongoose.model('User', userSchema);
module.exports.validate = validateUser;