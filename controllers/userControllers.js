const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth')
const Course = require('../models/Course');


//Check if email exists
module.exports.checkEmailExists = (reqBody) => {
	
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) return `${result[0].email} exists!`
  		
  		return 'Email not found!'
	})
}


//Registration
module.exports.registerUser = (reqBody) => {
 
			let newUser = new User({
			firstName: reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
			mobileNo: reqBody.mobileNo,
			password: bcrypt.hashSync(reqBody.password,10)
		})


	return newUser.save().then((user, err) => {

		if(err) return false
		
		return user
		
	})
}



//login
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null)  return 'Email not Found'

		const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

		if(!isPasswordCorrect) return 'Password is incorrect'

		return { access: auth.createAccessToken(result)}		 			
	})
}



 //DETAILS

//A C T I V I T Y S O L U T I O N

 module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {
	if(result == null) return 'UserId not found';
	result.password = "";

	return result;
 	});
};


// Enroll
//ACTIVITY 5 solution
module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId : data.courseId})
		
		return user.save().then((user, err) => {

			if(!user.isAdmin) return true

			return false
			
			if(err) return err
					
			
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId : data.userId})

		return course.save().then((course, err) => {

			if(err){

				 return false

			} else {

				return true
			}
		})
	})

	
	if(isUserUpdated && isCourseUpdated) return true

		return false
}




