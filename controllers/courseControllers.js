const Course = require('../models/Course');
const auth = require('../auth');

//Add a course
//ACTIVITY Solution

module.exports.addCourse = async(data) => {
console.log(data)
	
	if (!data.isAdmin) return 'Unauthorized user'

		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		return newCourse.save().then((course, error) => {

			if (error) return false;

			 return `Course added ${course}`;

		});
};


// retrieving all courses
module.exports.getAllCourses = async(user) => {

	if(!user.isAdmin) return `${user.email} is not authorized`

	return Course.find({}).then(result => {
		
	return result

	})
}


//retrieval of active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
}

//retrieval of a specific course

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}


//updating a course

module.exports.updateCourse = (data) => {
	console.log(data);
	return Course.findById(data.courseId).then((result,err) => {
		if(!data.payload.isAdmin) return 'Unauthorized user!'
		
			result.name = data.updatedCourse.name
			result.description = data.updatedCourse.description
			result.price = data.updatedCourse.price

			return result.save().then((updatedCourse,err) => {
				
				if(err) return false
				
				return updatedCourse
				
			})
	})
}



//ACTIVITY 3 SOLUTION
//archiving a course

module.exports.archiveCourse = (data) => {
	console.log(data);
	return Course.findById(data.courseId).then(res => {

		 	if(!data.payload.isAdmin) return 'Unauthorized'

			res.isActive = data.archivedCourse.isActive

			if(res.isActive) return false

			return res.save().then((archivedCourse, err) => {

			if(err) return ('Error',err);

			return 'Course successfuly archived!';
	
		})
	} )

}




