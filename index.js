const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const dotenv = require('dotenv');
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');

//Environment Variables
dotenv.config();
const secret = process.env.CONNECTION_STRING;

const app = express();
const port = process.env.PORT || 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


//Database Connect
mongoose.connect(secret, 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);


let db = mongoose.connection;

db.on('error', console.error.bind(console, "Connection Error"));
db.once('open', () => console.log("Successfully connected to MongoDB"));


app.use('/users', userRoutes);
app.use('/courses', courseRoutes);


app.get('/', (req, res) => {
	res.send('Hosted');
})
app.listen(port, () => console.log(`Server running at port ${port}`))
