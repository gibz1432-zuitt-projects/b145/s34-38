const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers')
const auth = require('../auth');
const User = require('../models/User');
// const validate = require('../models/User');

//Checking Email
router.post('/checkEmail', (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

// Registration
router.post('/register',(req,res) => {

const { error } = validate(req.body); 
if (error) return res.status(400).send(error.details[0].message);

	userController.registerUser(req.body)
	.then(resultFromController => res.send(resultFromController))
});

//Login
router.post('/login', (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Retrieve specific details

router.post("/details", auth.verify,(req, res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

router.post("/enroll", auth.verify,(req, res) => {

	

	let data = {

		userId : req.body.userId,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
})



module.exports = router;